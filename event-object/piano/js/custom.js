const middleSounds = ['sounds/middle/first.mp3', 'sounds/middle/second.mp3', 'sounds/middle/third.mp3', 'sounds/middle/fourth.mp3', 'sounds/middle/fifth.mp3'];
const lowerSounds = ['sounds/lower/first.mp3', 'sounds/lower/second.mp3', 'sounds/lower/third.mp3', 'sounds/lower/fourth.mp3', 'sounds/lower/fifth.mp3'];
const higherSounds = ['sounds/higher/first.mp3', 'sounds/higher/second.mp3', 'sounds/higher/third.mp3', 'sounds/higher/fourth.mp3', 'sounds/higher/fifth.mp3'];
const piano = document.getElementsByClassName('set')[0];
const keyList = document.getElementsByTagName('li');
const trackList = document.getElementsByTagName('audio');
let index = 0;

for (let track of trackList) {
    track.src = middleSounds[index++];
}

function play() {
    this.getElementsByTagName('audio')[0].currentTime = 0;
    this.getElementsByTagName('audio')[0].play();
}

for (let key of keyList) {
    key.addEventListener('click', play);
}

function keyPlay(event) {
    if (event.shiftKey) {
        piano.classList.remove('middle');
        piano.classList.add('lower');
        let index = 0;
        if (!event.repeat) {
            for (let track of trackList) {
                track.src = lowerSounds[index++];
            }
        }
    } else if (event.altKey) {
        piano.classList.remove('middle');
        piano.classList.add('higher');
        let index = 0;
        if (!event.repeat) {
            for (let track of trackList) {
                track.src = higherSounds[index++];
            }
        }
    } else {
        piano.classList.remove('lower');
        piano.classList.remove('higher');
        piano.classList.add('middle');
        let index = 0;
        for (let track of trackList) {
            track.src = middleSounds[index++];
        }
    }
}

document.addEventListener('keydown', keyPlay);
document.addEventListener('keyup', keyPlay);