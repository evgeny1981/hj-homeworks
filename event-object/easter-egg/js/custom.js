const nav = document.getElementsByTagName('nav')[0];
const secret = document.getElementsByClassName('secret')[0];
const pass = ['KeyY', 'KeyT', 'KeyN', 'KeyJ', 'KeyK', 'KeyJ', 'KeyU', 'KeyB', 'KeyZ'];
const userPass = [];
let count = 0;

function navMenu(event) {
    if (event.ctrlKey && event.altKey) {
        if (event.code === 'KeyT')
            nav.classList.toggle('visible');
    }
}

document.addEventListener('keydown', navMenu);


function openSecret(event) {
    if (event.code === pass[count]) {
        count++;
        if (count === pass.length) {
            secret.classList.add('visible');
        }
    } else {
        count = 0;
    }
}


document.addEventListener('keydown', openSecret);