const thumbs = document.getElementsByTagName('a');
const fullImg = document.getElementById('view');
const thumbsLinks = [];

for (let link of thumbs) {
    thumbsLinks.push(link.href);
}

function changeImages(event) {
    let href = this.href;
    let current = document.getElementsByClassName('gallery-current')[0];
    event.preventDefault();
    current.classList.remove('gallery-current');
    this.classList.add('gallery-current');
    fullImg.src = href;
}

Array.from(thumbs).forEach(link => {
    link.addEventListener('click', changeImages)
});