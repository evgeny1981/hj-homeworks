'use strict';

const listWS = document.querySelector('.websocket');
const itemsWS = listWS.getElementsByTagName('div');

const ws = new WebSocket('wss://neto-api.herokuapp.com/comet/websocket');
ws.addEventListener('open', () => {});

ws.addEventListener('message', event => {
    for (let itemWS of itemsWS) {
        if (itemWS.textContent === event.data) {
            itemWS.classList.add('flip-it');
        } else {
            itemWS.classList.remove('flip-it');
        }
    }
});