'use strict';

const list = document.querySelector('.pooling');
const items = list.getElementsByTagName('div');

function timer() {
    const xhr = new XMLHttpRequest();
    xhr.addEventListener('load', onLoad);
    xhr.open('GET', 'https://neto-api.herokuapp.com/comet/pooling');
    xhr.send();

    function onLoad() {
        for (let item of items) {
            if (item.textContent === xhr.responseText) {
                item.classList.add('flip-it');
            } else {
                item.classList.remove('flip-it');
            }
        }
    }
}

setInterval(timer, 5000);