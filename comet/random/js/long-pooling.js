'use strict';

const listLong = document.querySelector('.long-pooling');
const itemsLong = listLong.getElementsByTagName('div');
const xhrLong = new XMLHttpRequest();

xhrLong.open('GET', 'https://neto-api.herokuapp.com/comet/long-pooling');
xhrLong.send();

xhrLong.addEventListener('load', onload);

function onload() {
    if (xhrLong.readyState !== 4) return;
    let randomNum = parseInt(xhrLong.responseText);
    if (xhrLong.status >= 200) {
        for (let itemLong of itemsLong) {
            if (itemLong.textContent == randomNum) {
                itemLong.classList.add('flip-it');
            } else {
                itemLong.classList.remove('flip-it');
            }
        }
    } else {
        return;
    }
    xhrLong.addEventListener('load', onload);
    xhrLong.open('GET', 'https://neto-api.herokuapp.com/comet/long-pooling');
    xhrLong.send();
}