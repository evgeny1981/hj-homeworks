'use strict';

const titles = document.getElementsByTagName('th');
const wrapper = document.querySelector('table');

const handleTableClick = event => {
    if (event.target.tagName === 'TH') {
        let field = event.target.dataset.propName;
        let direction;
        if (event.target.dataset.dir > 0) {
            event.target.dataset.dir = -1;
        } else {
            event.target.dataset.dir = 1;
        }
        direction = event.target.dataset.dir;
        wrapper.dataset.sortBy = field;
        sortTable(field, direction);
    } else {
        return;
    }
}