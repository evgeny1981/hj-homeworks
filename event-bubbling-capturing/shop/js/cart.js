'use strict';

const changeCart = event => {
    if (event.target.classList.contains('add-to-cart')) {
        const obj = {};
        obj.title = event.target.dataset.title;
        obj.price = event.target.dataset.price;
        addToCart(obj);
    } else {
        return;
    }
};

list.addEventListener('click', changeCart);
