const playList = ['mp3/LA Chill Tour.mp3', 'mp3/LA Fusion Jam.mp3', 'mp3/This is it band.mp3'];
const titleList = ['LA Chill Tour', 'LA Fusion Jam', 'This is it band'];
const player = document.getElementsByClassName('mediaplayer')[0];
const track = document.getElementsByTagName('audio')[0];
const playButton = document.getElementsByClassName('playstate')[0];
const stopButton = document.getElementsByClassName('stop')[0];
const prevButton = document.getElementsByClassName('back')[0];
const nextButton = document.getElementsByClassName('next')[0];
const songTitle = document.getElementsByClassName('title')[0];
let index = 1;
let songIndex = 1;

const playPause = () => {
    player.classList.toggle('play');
    if (player.classList.contains('play')) {
        track.play();
    } else {
        track.pause();
    }
};

const stopAction = () => {
    player.classList.remove('play');
    track.pause();
    track.currentTime = 0;
};

const prevTrack = () => {
    if (index < 0) {
        index = playList.length - 1;
        if (songIndex < 0) {
            songIndex = titleList.length - 1;
        }
    }
    songTitle.title = titleList[songIndex--];
    track.src = playList[index--];
    if (player.classList.contains('play')) {
        track.play();
    } else {
        track.pause();
    }
};

const nextTrack = () => {
    if (index === playList.length) {
        index = 0;
        if (songIndex === titleList.length) {
            songIndex = 0;
        }
    }
    songTitle.title = titleList[songIndex++];
    track.src = playList[index++];
    if (player.classList.contains('play')) {
        track.play();
    } else {
        track.pause();
    }
};

playButton.onclick = playPause;
stopButton.onclick = stopAction;
prevButton.onclick = prevTrack;
nextButton.onclick = nextTrack;