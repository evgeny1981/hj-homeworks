const buttons = document.getElementsByTagName('li');

for (let button of buttons) {
    button.onclick = () => {
        button.getElementsByTagName('audio')[0].currentTime = 0;
        button.getElementsByTagName('audio')[0].play();
    }
}