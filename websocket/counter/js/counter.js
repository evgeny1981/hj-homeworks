'use strict';

const connection = new WebSocket('wss://neto-api.herokuapp.com/counter');
const counter = document.querySelector('.counter');
const errors = document.querySelector('.errors');

connection.addEventListener('open', () => {});

connection.addEventListener('message', event => {
    const dataObj = JSON.parse(event.data);
    counter.textContent = dataObj.connections;
    errors.textContent = dataObj.errors;
});

window.addEventListener('beforeunload', () => {
    connection.close(1000);
});