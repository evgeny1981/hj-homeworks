'use strict';

const connection = new WebSocket('wss://neto-api.herokuapp.com/mouse');

connection.addEventListener('open', () => {
    showBubbles(connection);
});

const chowClick = event => {
    const obj = {};
    obj.x = event.pageX;
    obj.y = event.pageY;
    connection.send(JSON.stringify(obj));
    showBubbles(connection);
};

window.addEventListener('click', chowClick);


