'use strict';

const chatContent = document.querySelector('.chat');
const chatForm = chatContent.querySelector('.message-box');
const messageInput = chatForm.querySelector('.message-input');
const messageSubmit = chatForm.querySelector('.message-submit');
const messageContent = chatContent.querySelector('.messages-content');
const chatStatus = chatContent.querySelector('.chat-status');
const messageStatus = chatContent.querySelector('.message-status');
const messagePersonal = chatContent.querySelector('.message-personal');
const messageLoad = chatContent.querySelector('.loading');
const messageUser = chatContent.querySelector('.loading + .message');
const now = new Date();

const connection = new WebSocket('wss://neto-api.herokuapp.com/chat');

connection.addEventListener('open', () => { 
    chatStatus.textContent = chatStatus.dataset.online;
    messageSubmit.removeAttribute('disabled');
    let status = messageStatus.cloneNode(true);
    let statusText = status.querySelector('.message-text');
    statusText.textContent = 'Пользователь появился в сети'; 
    messageContent.appendChild(status);
});

connection.addEventListener('message', event => {
    let message = messageUser.cloneNode(true);
    let text = message.querySelector('.message-text');
    let time = message.querySelector('.timestamp');
    if (event.data === '...') {
        let loading = messageLoad.cloneNode(true);
        messageContent.appendChild(loading);
    } else {
        let hours = now.getHours();
        let minutes = now.getMinutes();
        time.textContent = `${hours}:${minutes}`;
        text.textContent = event.data;
        messageContent.appendChild(message);
    }
});

const sendMessage = (event) => {
    event.preventDefault();
    if (messageInput.value !== '') {
        connection.send(messageInput.value);
        let message = messagePersonal.cloneNode(true);
        let text = message.querySelector('.message-text');
        let time = message.querySelector('.timestamp');
        let hours = now.getHours();
        let minutes = now.getMinutes();
        time.textContent = `${hours}:${minutes}`;
        text.textContent = messageInput.value;
        messageContent.appendChild(message);
    } 
};

messageSubmit.addEventListener('click', sendMessage);