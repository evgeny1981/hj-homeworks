'use strict';

const commentsContainer = document.querySelector('.comments');

function showComments(list) {
    commentsContainer.appendChild(
        templateComment(list.map(comment => commentJSTemplate(comment)))
    );
}

function commentJSTemplate(comment) {
    return {
        tag: 'div',
        cls: 'comment-wrap',
        content: [{
                tag: 'div',
                cls: 'photo',
                attrs: {
                    title: comment.author.name
                },
                content: {
                    tag: 'div',
                    cls: 'avatar',
                    attrs: {
                        style: `background-image: url('${comment.author.pic}')`
                    }
                }
            },
            {
                tag: 'div',
                cls: 'comment-block',
                content: [{
                        tag: 'p',
                        cls: 'comment-text',
                        content: comment.text.split('<br>').join('\n')
                    },
                    {
                        tag: 'div',
                        cls: 'bottom-comment',
                        content: [{
                                tag: 'div',
                                cls: 'comment-date',
                                content: new Date(comment.date).toLocaleString('ru-Ru')
                            },
                            {
                                tag: 'ul',
                                cls: 'comment-actions',
                                content: [{
                                        tag: 'li',
                                        cls: 'complain',
                                        content: 'Пожаловаться'
                                    },
                                    {
                                        tag: 'li',
                                        cls: 'reply',
                                        content: 'Ответить'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
}

function templateComment(block) {
    if ((block === undefined) || (block === null) || (block === false)) {
        return document.createTextNode('');
    }

    if ((typeof block === 'string') || (typeof block === 'number') || (block === true)) {
        return document.createTextNode(block);
    }

    if (Array.isArray(block)) {
        return block.reduce((f, elem) => {
            f.appendChild(templateComment(elem));
            return f;
        }, document.createDocumentFragment());
    }

    let element = document.createElement(block.tag || 'div');
    [].concat(block.cls || []).forEach(className => element.classList.add(className));
    if (block.attrs) {
        Object.keys(block.attrs).forEach(key => element.setAttribute(key, block.attrs[key]))
    }

    element.appendChild(templateComment(block.content));

    return element;
}

fetch('https://neto-api.herokuapp.com/comments')
    .then(res => res.json())
    .then(showComments);