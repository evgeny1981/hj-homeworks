'use strict';

const counter = document.getElementById('counter');
const increment = document.getElementById('increment');
const decrement = document.getElementById('decrement');
const reset = document.getElementById('reset');

let count = 0;
localStorage.count;
counter.textContent = localStorage.count;

const plus = () => {
    if (localStorage.count === undefined) {
        counter.textContent = count;
        count++;
    } else {
        count = localStorage.count;
        count++;
        localStorage.count = count;
        counter.textContent = localStorage.count;
    }
};

const minus = () => {
    count = localStorage.count;
    if (counter.textContent > 0) {
        count--;
        counter.textContent = count;
    }
    localStorage.count = count;
};

const resetCount = () => {
    if (counter.textContent > 0) {
        count = 0;
        counter.textContent = count;
    }
    localStorage.count = count;
};

increment.addEventListener('click', plus);
decrement.addEventListener('click', minus);
reset.addEventListener('click', resetCount);