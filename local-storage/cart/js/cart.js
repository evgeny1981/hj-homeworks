'use strict';

const colorsContent = document.getElementById('colorSwatch');
const sizesContent = document.getElementById('sizeSwatch');
const cartContent = document.getElementById('quick-cart');
const productForm = document.getElementById('AddToCartForm');
const addButton = document.getElementById('AddToCart');

const xhrColors = new XMLHttpRequest();
xhrColors.addEventListener('load', onLoadColors);
xhrColors.open('GET', 'https://neto-api.herokuapp.com/cart/colors');
xhrColors.send();

function onLoadColors() {
    const data = JSON.parse(xhrColors.responseText);
    for (const color of data) {
        let snippet = `
            <div data-value="${color.type}" class="swatch-element color ${color.type} ${color.isAvailable ? 'available' : 'soldout'}">
                <div class="tooltip">${color.title}</div>
                <input quickbeam="color" id="swatch-1-${color.type}" type="radio" name="color" value="${color.type}" ${!color.isAvailable ? 'disabled' : ''} ${color.type === 'red' ? 'checked' : ''}>
                <label for="swatch-1-${color.type}" style="border-color: red;">
                    <span style="background-color: ${color.code};"></span>
                    <img class="crossed-out" src="https://neto-api.herokuapp.com/hj/3.3/cart/soldout.png?10994296540668815886">
                </label>
            </div>
        `;
        colorsContent.innerHTML += snippet;
    }
}

const xhrSizes = new XMLHttpRequest();
xhrSizes.addEventListener('load', onLoadSizes);
xhrSizes.open('GET', 'https://neto-api.herokuapp.com/cart/sizes');
xhrSizes.send();

function onLoadSizes() {
    const data = JSON.parse(xhrSizes.responseText);
    for (const size of data) {
        let snippet = `
            <div data-value="${size.type}" class="swatch-element plain ${size.type} ${size.isAvailable ? 'available' : 'soldout'}">
                <input id="swatch-0-${size.type}" type="radio" name="size" value="${size.type}" ${!size.isAvailable ? 'disabled' : ''} ${size.type === 'xl' ? 'checked' : ''}>
                <label for="swatch-0-${size.type}">
                    ${size.title}
                    <img class="crossed-out" src="https://neto-api.herokuapp.com/hj/3.3/cart/soldout.png?10994296540668815886">
                </label>
            </div>
        `;
        sizesContent.innerHTML += snippet;
    }
}

const xhrCart = new XMLHttpRequest();
xhrCart.addEventListener('load', onLoadCart);
xhrCart.open('GET', 'https://neto-api.herokuapp.com/cart');
xhrCart.send();

function onLoadCart() {
    const data = JSON.parse(xhrCart.responseText);
    console.log(data);
    let snippet = `
        <div class="quick-cart-product quick-cart-product-static" id="quick-cart-product-${data.id}" style="opacity: 1;">
            <div class="quick-cart-product-wrap">
                <img src="${data.pic}" title="${data.title}">
                <span class="s1" style="background-color: #000; opacity: .5">$800.00</span>
                <span class="s2"></span>
            </div>
            <span class="count hide fadeUp" id="quick-cart-product-count-${data.id}">${data.quantity}</span>
            <span class="quick-cart-product-remove remove" data-id="${data.id}"></span>
        </div>
        <a id="quick-cart-pay" quickbeam="cart-pay" class="cart-ico open">
            <span>
                <strong class="quick-cart-text">Оформить заказ<br></strong>
                <span id="quick-cart-price">$800.00</span>
            </span>
        </a>
    `;
    cartContent.innerHTML = snippet;
}

const addCart = (event) => {
    event.preventDefault();
    const productId = productForm.dataset.productId;
    const formData = new FormData(productForm);
    formData.append('productId', productId);
    const xhr = new XMLHttpRequest();
    xhr.addEventListener('load', onLoad);
    xhr.open('POST', 'https://neto-api.herokuapp.com/cart');
    xhr.setRequestHeader('Content-Type', 'application/json');
    const dataObj = {};
    for (const [k, v] of formData) {
        dataObj[k] = v;
    }
    console.log(dataObj);
    xhr.send(JSON.stringify(dataObj));

    function onLoad() {
        const data = JSON.parse(xhr.responseText);
        console.log(data);
    }
};

addButton.addEventListener('click', addCart);