'use strict';

const loginForm = document.querySelector('.sign-in-htm');
const regForm = document.querySelector('.sign-up-htm');
const loginButton = loginForm.querySelector('.button');
const regButton = regForm.querySelector('.button');
const loginOutput = loginForm.querySelector('.error-message');
const regOutput = regForm.querySelector('.error-message');

const loginSubmit = (event) => {
    event.preventDefault();
    const formData = new FormData(loginForm);
    const xhr = new XMLHttpRequest();
    xhr.addEventListener('load', onLoad);
    xhr.open('POST', 'https://neto-api.herokuapp.com/signin');
    xhr.setRequestHeader('Content-Type', 'application/json');
    const dataObj = {};
    for (const [k, v] of formData) {
        dataObj[k] = v;
    }
    xhr.send(JSON.stringify(dataObj));

    function onLoad() {
        const data = JSON.parse(xhr.responseText);
        if (data.error) {
            loginOutput.textContent = data.message;
        } else {
            loginOutput.textContent = `Пользователь ${data.name} успешно авторизован`;
        }
    }
};

const regSubmit = (event) => {
    event.preventDefault();
    const formData = new FormData(regForm);
    const xhr = new XMLHttpRequest();
    xhr.addEventListener('load', onLoad);
    xhr.open('POST', 'https://neto-api.herokuapp.com/signup');
    xhr.setRequestHeader('Content-Type', 'application/json');
    const dataObj = {};
    for (const [k, v] of formData) {
        dataObj[k] = v;
    }
    xhr.send(JSON.stringify(dataObj));

    function onLoad() {
        const data = JSON.parse(xhr.responseText);
        if (data.error) {
            regOutput.textContent = data.message;
        } else {
            regOutput.textContent = `Пользователь ${data.name} успешно зарегистрирован`;
        }
    }
};

loginButton.addEventListener('click', loginSubmit);
regButton.addEventListener('click', regSubmit);