const slider = document.getElementById('currentPhoto');
const images = ['i/breuer-building.jpg', 'i/guggenheim-museum.jpg', 'i/headquarters.jpg', 'i/IAC.jpg', 'i/new-museum.jpg'];
const prevButton = document.getElementById('prevPhoto');
const nextButton = document.getElementById('nextPhoto');
let index = 1;

slider.src = images[0];

const prevImg = () => {
    if (index < 0) {
        index = images.length - 1;
    }
    slider.src = images[index--];
}

const nextImg = () => {
    if (index === images.length) {
        index = 0;
    }
    slider.src = images[index++];
}

nextButton.onclick = nextImg;
prevButton.onclick = prevImg;
