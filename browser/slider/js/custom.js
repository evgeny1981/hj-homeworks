const slider = document.getElementById('slider'); 
const images = ['i/airmax-jump.png', 'i/airmax-on-foot.png', 'i/airmax-playground.png', 'i/airmax-top-view.png', 'i/airmax.png']; 
let index = 1; 

slider.src = images[0]; 

setInterval(() => { 
    if (index === images.length) {
        index = 0;
    }
    slider.src = images[index++];
}, 5000);