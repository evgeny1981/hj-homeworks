const slides = document.querySelector('.slides');
const fisrtSlide = slides.querySelector('.slide:first-child');
const lastSlide = slides.querySelector('.slide:last-child');
const sliderNav = document.getElementsByTagName('a');
const nextButton = document.querySelector('a[data-action=next]');
const prevButton = document.querySelector('a[data-action=prev]');
const lastButton = document.querySelector('a[data-action=last]');
const firstButton = document.querySelector('a[data-action=first]');

fisrtSlide.classList.add('slide-current');

if (fisrtSlide) {
    prevButton.classList.add('disabled');
    firstButton.classList.add('disabled');
}

const changeSlide = event => {
    const current = slides.querySelector('.slide-current');
    const nextSlide = current.nextElementSibling;
    const prevSlide = current.previousElementSibling;
    if (event.target.dataset.action === 'next') {
        prevButton.classList.remove('disabled');
        firstButton.classList.remove('disabled');
        if (nextSlide === null) {
            return;
        }
        current.classList.remove('slide-current');
        nextSlide.classList.add('slide-current');
        if (lastSlide.classList.contains('slide-current')) {
            event.currentTarget.classList.add('disabled');
            lastButton.classList.add('disabled');
        }
    } else if (event.target.dataset.action === 'prev') {
        nextButton.classList.remove('disabled');
        lastButton.classList.remove('disabled');
        if (prevSlide === null) {
            return;
        }
        current.classList.remove('slide-current');
        prevSlide.classList.add('slide-current');
        if (fisrtSlide.classList.contains('slide-current')) {
            event.currentTarget.classList.add('disabled');
            firstButton.classList.add('disabled');
        }
    } else if (event.target.dataset.action === 'last') {
        current.classList.remove('slide-current');
        lastSlide.classList.add('slide-current');
        event.currentTarget.classList.add('disabled');
        nextButton.classList.add('disabled');
        prevButton.classList.remove('disabled');
        firstButton.classList.remove('disabled');
    } else if (event.target.dataset.action === 'first') {
        current.classList.remove('slide-current');
        fisrtSlide.classList.add('slide-current');
        event.currentTarget.classList.add('disabled');
        prevButton.classList.add('disabled');
        nextButton.classList.remove('disabled');
        lastButton.classList.remove('disabled');
    }
};

for (let button of sliderNav) {
    button.addEventListener('click', changeSlide);
}