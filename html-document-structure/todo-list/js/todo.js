const done = document.querySelector('.done');
const undone = document.querySelector('.undone');
const labels = document.getElementsByTagName('label');

function changeList(e) {
    const item = e.currentTarget;
    const input = item.querySelector('input');
    if (input.checked === true) {
        done.appendChild(item);
    } else {
        undone.appendChild(item);
    }
}

for (let label of labels) {
    label.addEventListener('change', changeList);
}