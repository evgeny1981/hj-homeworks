const tabs = document.querySelector('.tabs-nav');
const content = document.querySelector('.tabs-content');

const listContent = content.children;
const itemTab = tabs.querySelector('li');
const cloneItemTab = tabs.removeChild(itemTab);

for (let article of listContent) {
    let tabsButtonClone = cloneItemTab.cloneNode(true);
    tabs.appendChild(tabsButtonClone);
    let tabLink = tabsButtonClone.querySelector('a');
    tabLink.textContent = article.dataset.tabTitle;
    tabLink.classList.add(article.dataset.tabIcon);
    article.classList.add('hidden');
}

tabs.firstElementChild.classList.add('ui-tabs-active');
content.firstElementChild.classList.remove('hidden');

const tabButtons = tabs.children;

const tabActions = event => {
    let current = tabs.querySelector('.ui-tabs-active');
    current.classList.remove('ui-tabs-active');
    event.currentTarget.classList.add('ui-tabs-active');
    for (let article of listContent) {
        article.classList.add('hidden');
        if (event.currentTarget.textContent === article.dataset.tabTitle) {
            article.classList.remove('hidden');
        }
    }
}

for (let tab of tabButtons) {
    tab.addEventListener('click', tabActions);
}