'use strict';

const canvas = document.querySelector('canvas');
const ctx = canvas.getContext('2d');

const getRandom = (min, max) => {
    return Math.random() * (max - min) + min;
};

const loadStars = () => {
    const countsStars = getRandom(200, 400).toFixed();
    const colors = ['#ffffff', '#ffe9c4', '#d4fbff'];
    const countsArray = [];
    countsArray.length = countsStars;
    ctx.beginPath();
    for (const item of countsArray) {
        const positionX = getRandom(0, ctx.canvas.width).toFixed();
        const positionY = getRandom(0, ctx.canvas.height).toFixed();
        const colorItem = getRandom(0, 2).toFixed();
        const brightness = getRandom(0.8, 1).toFixed(1);
        const size = getRandom(0, 1.1).toFixed(1);
        const getColor = colors[colorItem];
        ctx.fillRect(positionX, positionY, size, size);
        ctx.fillStyle = getColor;
        ctx.globalAlpha = brightness;
    }
};

loadStars();

const renderStars = () => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    loadStars();
};

canvas.addEventListener('click', renderStars);