'use strict';

const canvas = document.querySelector('canvas');
const ctx = canvas.getContext('2d');
const PI = Math.PI;

let curves = [];
let drawing = false;
let needsRepaint = false;
let radius = 100;
let hue = 0;

ctx.canvas.width = document.documentElement.clientWidth;
ctx.canvas.height = document.documentElement.clientHeight;

function changeSize() {
    curves = [];
    needsRepaint = true;
    ctx.canvas.width = document.documentElement.clientWidth;
    ctx.canvas.height = document.documentElement.clientHeight;
}

function hueStyle(e) {
    if (e.shiftKey) {
        hue--;
    } else {
        hue++;
    }
    if (hue > 359) {
        hue = 0;
    }
    if (hue < 0) {
        hue = 359;
    }
    ctx.fillStyle = `hsl(${hue}, 100%, 50%)`;
    ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;
}

function brushRadius() {
    if (radius >= 100) {
        radius--;
    } else if (radius <= 5) {
        radius++;
    }
    ctx.lineWidth = radius;
}

function circle(point) {
    ctx.beginPath();
    ctx.arc(...point, brushRadius() / 2, 0, 2 * PI);
    ctx.fill();
}

function smoothCurveBetween(p1, p2) {
    const cp = p1.map((coord, idx) => (coord + p2[idx]) / 2);
    ctx.quadraticCurveTo(...p1, ...cp);
}

function smoothCurve(points) {
    ctx.beginPath();
    brushRadius();
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    ctx.moveTo(...points[0]);
    for (let i = 1; i < points.length - 1; i++) {
        smoothCurveBetween(points[i], points[i + 1]);
    }
    ctx.stroke();
}

function makePoint(x, y, reflect = false) {
    return reflect ? [y, x] : [x, y];
};

canvas.addEventListener("mousedown", (e) => {
    drawing = true;
    const curve = [];
    curve.push(makePoint(e.offsetX, e.offsetY));
    curves.push(curve);
    needsRepaint = true;
});

canvas.addEventListener("mouseup", () => {
    drawing = false;
});

canvas.addEventListener("mouseleave", () => {
    drawing = false;
});

canvas.addEventListener("mousemove", (e) => {
    if (drawing) {
        const point = makePoint(e.offsetX, e.offsetY);
        curves[curves.length - 1].push(point);
        needsRepaint = true;
    }
    brushRadius();
});

function repaint() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    curves.forEach((curve) => {
        circle(curve[0]);
        smoothCurve(curve);
    });
}

function tick() {
    if (needsRepaint) {
        repaint();
        needsRepaint = false;
    }
    window.requestAnimationFrame(tick);
    window.requestAnimationFrame(hueStyle);
}

tick();

window.addEventListener('resize', changeSize);
