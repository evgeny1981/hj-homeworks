const content = document.getElementById('content');
const xhr = new XMLHttpRequest();
content.innerHTML = '';
xhr.addEventListener('load', onLoad);
xhr.open('GET', 'https://neto-api.herokuapp.com/book/');
xhr.send();

function onLoad() {
    const data = JSON.parse(xhr.responseText);
    for (let book of data) {
        let cover = book.cover.small;
        content.innerHTML += `<li><img src=${cover}></li>`;
    }
    const items = document.querySelectorAll('#content li');
    for (let item = 0; item < items.length; item++) {
        items[item].dataset.title = data[item].title;
        items[item].dataset.author = data[item].author.name;
        items[item].dataset.info = data[item].info;
        items[item].dataset.price = data[item].price;
    }
}