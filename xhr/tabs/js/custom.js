const tabEmail = document.querySelector('nav a:first-child');
const tabSMS = document.querySelector('nav a:last-child');
const contentEmail = tabEmail.href;
const contentSMS = tabSMS.href;
const mainContent = document.getElementById('content');
const preloader = document.getElementById('preloader');
const xhr = new XMLHttpRequest();

function loadDefault() {
    xhr.addEventListener('load', onLoad);
    xhr.addEventListener('loadstart', onLoadStart);
    xhr.addEventListener('loadend', onLoadEnd);
    xhr.open('GET', contentEmail);
    xhr.send();

    function onLoad() {
        const data = xhr.responseText;
        mainContent.innerHTML = data;
    }

    function onLoadStart() {
        preloader.classList.remove('hidden');
    }

    function onLoadEnd() {
        preloader.classList.add('hidden');
    }
}

loadDefault();

function loadEmail(event) {
    event.preventDefault();
    tabEmail.classList.add('active');
    tabSMS.classList.remove('active');
    loadDefault();
}

function loadSMS(event) {
    event.preventDefault();
    tabSMS.classList.add('active');
    tabEmail.classList.remove('active');
    xhr.addEventListener('load', onLoad);
    xhr.addEventListener('loadstart', onLoadStart);
    xhr.addEventListener('loadend', onLoadEnd);
    xhr.open('GET', contentSMS);
    xhr.send();

    function onLoad() {
        const data = xhr.responseText;
        mainContent.innerHTML = data;
    }

    function onLoadStart() {
        preloader.classList.remove('hidden');
    }

    function onLoadEnd() {
        preloader.classList.add('hidden');
    }
}

tabEmail.addEventListener('click', loadEmail);
tabSMS.addEventListener('click', loadSMS);