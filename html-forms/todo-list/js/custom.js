const content = document.querySelector('.list-block');
const checkList = document.querySelectorAll('input');
const output = document.querySelector('output');
const fullCount = checkList.length;

let defArr = [];

for (let check of checkList) {
    if (check.checked === true) {
        defArr.push(check);
        output.value = `${defArr.length} из ${fullCount}`
    }
    check.addEventListener('click', inputCheck);
}

let count = defArr.length;

function inputCheck() {
    if (this.checked === true) {
        count++;
    } else {
        count--;
    }
    output.value = `${count} из ${fullCount}`;
    if (count === fullCount) {
        content.classList.add('complete');
    } else {
        content.classList.remove('complete');
    }
}