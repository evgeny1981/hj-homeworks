const content = document.getElementById('content');
const preloader = document.getElementById('loader');
const from = document.getElementById('from');
const to = document.getElementById('to');
const result = document.getElementById('result');
const source = document.getElementById('source');
const selects = document.querySelectorAll('select');
const xhr = new XMLHttpRequest();

xhr.addEventListener('load', onLoad);
xhr.addEventListener('loadstart', onLoadStart);
xhr.addEventListener('loadend', onLoadEnd);
xhr.open('GET', 'https://neto-api.herokuapp.com/currency');
xhr.send();

function onLoad() {
    const data = JSON.parse(xhr.responseText);
    for (let item of data) {
        let code = item.code;
        let value = item.value;
        from.innerHTML += `<option value="${value}">${code}</option>`;
        to.innerHTML += `<option value="${value}">${code}</option>`;
    }

    let resultVal = (Math.round(((from.value / to.value) * source.value) * 100)) / 100;
    result.value = resultVal;

    function changeVal() {
        resultVal = (Math.round(((from.value / to.value) * source.value) * 100)) / 100;
        result.value = resultVal;
    }
    for (let select of selects) {
        select.addEventListener('input', changeVal);
    }
    source.addEventListener('input', changeVal);
}



function onLoadStart() {
    preloader.classList.remove('hidden');
}

function onLoadEnd() {
    content.classList.remove('hidden');
    preloader.classList.add('hidden');
}