const form = document.querySelector('.contentform');
const output = document.getElementById('output');
const fields = document.getElementsByTagName('input');

const fieldLastName = document.querySelector('input[name="lastname"]');
const fieldName = document.querySelector('input[name="name"]');
const fieldEmail = document.querySelector('input[name="email"]');
const fieldCompany = document.querySelector('input[name="company"]');
const fieldAddress = document.querySelector('input[name="address"]');
const fieldZip = document.querySelector('input[name="zip"]');
const fieldCity = document.querySelector('input[name="city"]');
const fieldPhone = document.querySelector('input[name="phone"]');
const fieldRole = document.querySelector('input[name="role"]');
const fieldSubject = document.querySelector('input[name="subject"]');
const fieldMessage = document.querySelector('textarea[name="message"]');

const mesName = document.getElementById('name');
const mesLastName = document.getElementById('lastname');
const mesCompany = document.getElementById('company');
const mesRole = document.getElementById('role');
const mesZip = document.getElementById('zip');
const mesCity = document.getElementById('city');
const mesAddress = document.getElementById('address');
const mesSubject = document.getElementById('subject');
const mesMessage = document.getElementById('message');

const submit = document.querySelector('form .button-contact');
const edit = document.querySelector('main .button-contact');

const allFields = Array.from(fields);
allFields.push(fieldMessage);

fieldZip.type = 'number';

function validateForm() {
    if (fieldLastName.value !== '' && fieldName.value !== '' && fieldEmail.value !== '' && fieldCompany.value !== '' && fieldAddress.value !== '' && fieldZip.value !== '' && fieldCity.value !== '' && fieldPhone.value !== '' && fieldRole.value !== '' && fieldSubject.value !== '' && fieldMessage.value !== '') {
        submit.removeAttribute('disabled');
    } else {
        submit.setAttribute('disabled', 'disabled');
    }
}

for (let field of allFields) {
    field.addEventListener('input', validateForm);
}

function showMessage(event) {
    event.preventDefault();
    mesName.value = fieldName.value;
    mesLastName.value = fieldLastName.value;
    mesCompany.value = fieldCompany.value;
    mesRole.value = fieldRole.value;
    mesZip.value = fieldZip.value;
    mesCity.value = fieldCity.value;
    mesAddress.value = fieldAddress.value;
    mesSubject.value = fieldSubject.value;
    mesMessage.value = fieldMessage.value;
    form.classList.add('hidden');
    output.classList.remove('hidden');
}

function showForm() {
    form.classList.remove('hidden');
    output.classList.add('hidden');
}

submit.addEventListener('click', showMessage);
edit.addEventListener('click', showForm);