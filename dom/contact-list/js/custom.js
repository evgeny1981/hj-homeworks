function contactsLoad() {
    const contactsData = JSON.parse(loadContacts());
    const contactsList = document.querySelector('.contacts-list');
    contactsList.innerHTML = '';
    for (let contact of contactsData) {
        let name = contact.name;
        let email = contact.email;
        let phone = contact.phone;
        contactsList.innerHTML += `<li data-email="${email}" data-pfone="${phone}"><strong>${name}</strong></li>`;
    }
}

document.addEventListener('DOMContentLoaded', contactsLoad);


