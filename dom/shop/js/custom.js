function addCard() {
    const addButtons = document.querySelectorAll('button.add');
    const counts = document.getElementById('cart-count');
    const fullPrice = document.getElementById('cart-total-price');
    let count = 0;
    let resultPrice = 0;
    function addProduct(event) {
        counts.innerText = ++count;
        resultPrice += parseInt(event.currentTarget.dataset.price);
        fullPrice.innerText = getPriceFormatted(resultPrice);
    }
    for (button of addButtons) {
        let price = button.dataset.price;
        button.addEventListener('click', addProduct);
    }
}

document.addEventListener('DOMContentLoaded', addCard);