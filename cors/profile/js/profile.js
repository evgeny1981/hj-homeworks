'use strict';

const content = document.querySelector('.content');
const name = document.querySelector('[data-name]');
const description = document.querySelector('[data-description]');
const pic = document.querySelector('[data-pic]');
const position = document.querySelector('[data-position]');
const technologies = document.querySelector('[data-technologies]');

function showObj(obj) {
    name.textContent = obj.name;
    description.textContent = obj.description;
    pic.src = obj.pic;
    position.textContent = obj.position;

    function showTech(objTech) {
        objTech.forEach(item => {
            let techItem = document.createElement('span');
            techItem.className = `devicons devicons-${item}`;
            technologies.appendChild(techItem);
        });
    }

    loadTech(`https://neto-api.herokuapp.com/profile/${obj.id}/technologies`)
        .then(showTech);
    
    function loadTech(url) {
        return new Promise((done, fail) => {
            const numRandom = randName();
            window[numRandom] = done;
            const script = document.createElement('script');
            script.src = `${url}?jsonp=${numRandom}`;
            document.body.appendChild(script);
        });
    }

    content.style.display = 'initial';
}

loadData('https://neto-api.herokuapp.com/profile/me')
    .then(showObj);

function randName() {
    function getRandom(min, max) {
        return Math.random() * (max - min) + min;
    }
    let numRandom = getRandom(1, 10000);
    return `callback${numRandom.toFixed()}`;
}

function loadData(url) {
    return new Promise((done, fail) => {
        const numRandom = randName();
        window[numRandom] = done;
        const script = document.createElement('script');
        script.src = `${url}?jsonp=${numRandom}`;
        document.body.appendChild(script);
    });
}