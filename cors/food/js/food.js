'use strict';

const pic = document.querySelector('[data-pic]');
const title = document.querySelector('[data-title]');
const ingredients = document.querySelector('[data-ingredients]');
const rating = document.querySelector('[data-rating]');
const star = document.querySelector('[data-star]');
const votes = document.querySelector('[data-votes]');
const consumers = document.querySelector('[data-consumers]');

function randName() {
    function getRandom(min, max) {
        return Math.random() * (max - min) + min;
    }
    let numRandom = getRandom(1, 10000);
    return `callback${numRandom.toFixed()}`;
}


function showData(obj) {
    pic.style.backgroundImage = `url(${obj.pic})`;
    title.textContent = obj.title;
    const ingredientsList = obj.ingredients.join(', ');
    ingredients.textContent = `${ingredientsList}.`;    
}

function showRating(obj) {
    rating.textContent = obj.rating;
    const ratingWidth = obj.rating.toString().replace(/[.]/g, '');
    star.style.width = `${ratingWidth}%`;
    votes.textContent = `${obj.votes} оценок`;
}

function showUsers(obj) {
    obj.consumers.forEach(el => {
        const user = document.createElement('img');
        user.src = el.pic;
        user.title = el.name;
        consumers.appendChild(user);
    });
    const total = document.createElement('span');
    total.textContent = `(+${obj.total})`;
    consumers.appendChild(total);
}

function loadData(url) {
    return new Promise((done, fail) => {
        const numRandom = randName();
        window[numRandom] = done;
        const script = document.createElement('script');
        script.src = `${url}?jsonp=${numRandom}`;
        document.body.appendChild(script);
    });
}

function loadRating(url) {
    return new Promise((done, fail) => {
        const numRandom = randName();
        window[numRandom] = done;
        const script = document.createElement('script');
        script.src = `${url}?jsonp=${numRandom}`;
        document.body.appendChild(script);
    });
}

function loadUsers(url) {
    return new Promise((done, fail) => {
        const numRandom = randName();
        window[numRandom] = done;
        const script = document.createElement('script');
        script.src = `${url}?jsonp=${numRandom}`;
        document.body.appendChild(script);
    });
}

loadData('https://neto-api.herokuapp.com/food/42')
    .then(showData);

loadRating('https://neto-api.herokuapp.com/food/42/rating')
    .then(showRating);

loadUsers('https://neto-api.herokuapp.com/food/42/consumers')
    .then(showUsers);
