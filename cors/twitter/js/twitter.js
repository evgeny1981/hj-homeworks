'use strict';

const wallpapper = document.querySelector('[data-wallpaper]');
const username = document.querySelector('[data-username]');
const description = document.querySelector('[data-description]');
const pic = document.querySelector('[data-pic]');
const tweets = document.querySelector('[data-tweets]');
const followers = document.querySelector('[data-followers]');
const following = document.querySelector('[data-following]');

function showObj(obj) {
    wallpapper.src = obj.wallpaper;
    username.textContent = obj.username;
    description.textContent = obj.description;
    pic.src = obj.pic;
    tweets.textContent = obj.tweets;
    followers.textContent = obj.followers;
    following.textContent = obj.following;
}

loadData('https://neto-api.herokuapp.com/twitter/jsonp')
    .then(showObj);

function randName() {
    function getRandom(min, max) {
        return Math.random() * (max - min) + min;
    }
    let numRandom = getRandom(1, 10000);
    return `callback${numRandom.toFixed()}`;
}

function loadData(url) {
    return new Promise((done, fail) => {
        const numRandom = randName();
        window[numRandom] = done;
        const script = document.createElement('script');
        script.src = `${url}?jsonp=${numRandom}`;
        document.body.appendChild(script);
    });
}
